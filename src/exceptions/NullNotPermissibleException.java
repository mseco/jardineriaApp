package exceptions;

public class NullNotPermissibleException extends Throwable{

    private String message;
    
    public NullNotPermissibleException(String message) {
        this.message = message;
    }

    public String toString(){
        return message;
    }
}
