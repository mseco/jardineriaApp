package exceptions;

public class DateException extends Throwable{

    private String message;
    
    public DateException(String message) {
        this.message = message;
    }

    public String toString(){
        return message;
    }
}
