package util;

import java.sql.*;
import javax.swing.*;

public class Connection {

    private static Connection connection;
    
    public static Connection instance(){
        if (connection == null){ 
            connection = new Connection();
        }
        return connection;
    }
    
    private java.sql.Connection connectionSlave;
    
    private String state;
    
    private Connection(){
        this.state="close";
    }
    
    public void open(){
        assert this.state.equals("close");   
        try{
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            this.connectionSlave = DriverManager.getConnection("jdbc:mysql://localhost:3306/jardineria","root",""); 
            this.state="open";
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, 
                                "No ha sido posible conectarse a la base de datos", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
    }
    
    public void close(){
        assert this.state.equals("open");
        try {
            this.connectionSlave.close();
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Ha habido un error al cerrar la base de datos", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
        this.state="close";
    }
    
    public PreparedStatement prepareStatement(String sql) {
        assert this.state.equals("open");
        try {
            return this.connectionSlave.prepareStatement(sql);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Una de las consultas no ha podido realizarse", 
                                "Error de consulta", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
        return null;
    }
    
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) {
        assert this.state.equals("open");
        try {
            return this.connectionSlave.prepareStatement(sql, resultSetType, resultSetConcurrency);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Una de las consultas no ha podido realizarse", 
                                "Error de consulta", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
        return null;
    }
}
