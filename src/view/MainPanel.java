package view;

import java.awt.*;
import javax.swing.*;

class MainPanel extends JPanel{

    private NavegationPanel navegationPanel;
    
    private BodyMainPanel bodyMainPanel;
    
    MainPanel(){
        bodyMainPanel = new BodyMainPanel();
        navegationPanel= new NavegationPanel(bodyMainPanel);
        
        this.setLayout(new BorderLayout());
        this.add(navegationPanel, BorderLayout.WEST);
        this.add(bodyMainPanel, BorderLayout.CENTER);
    }
}
