package view;

import util.Connection;
import java.awt.event.*;
import javax.swing.*;

public class FrameJardineriaApp extends JFrame{

    public FrameJardineriaApp(){
        this.add(new MainPanel());
    }
    
    public void init(){
        this.setSize(800,600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                Connection.instance().close();
                System.exit(0);
            }
        });        
        this.setVisible(true);
    }
    
}
