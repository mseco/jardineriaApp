package view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
        
class NavegationPanel extends JPanel implements ActionListener{

    private BodyMainPanel bodyMainPanel;
    
    public NavegationPanel(BodyMainPanel bodyMainPanel) {
        this.bodyMainPanel=bodyMainPanel;
                
        JButton button1 = new JButton("Administración");
        JButton button2 = new JButton("Consultas");
        this.setLayout(new GridLayout(2,1));
        this.add(button1);
        this.add(button2);
        button1.addActionListener(this);
        button2.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String buttonActivated = e.getActionCommand();
        if (buttonActivated.equals("Administración")){
            bodyMainPanel.show(buttonActivated);
        }else if (buttonActivated.equals("Consultas")){
            bodyMainPanel.show(buttonActivated);
        }
    }
}
