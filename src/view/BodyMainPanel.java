package view;

import view.administrativeMenu.*;
import view.queriesMenu.*;
import java.awt.*;
import javax.swing.*;

class BodyMainPanel extends JPanel{
    
    private CardLayout cardLayout;

    public BodyMainPanel() {
        cardLayout=new CardLayout();
                
        this.setLayout(cardLayout);
        this.add(new JPanel(), "Default");        
        this.add(new AdministrativeTabbedPanel(), "Administración");
        this.add(new QueriesPanel(), "Consultas");
    }

    void show(String buttonActivated) {
        cardLayout.show(this, buttonActivated);
    }
    
}
