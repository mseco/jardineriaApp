package view.administrativeMenu;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import data.ModificableSQLQuery;
import exceptions.*;
import java.sql.SQLException;
import view.scrollPanel.EmptyModificableTuplePanel;

class InsertionPanel extends JPanel{

    private ModificableSQLQuery modificableSQLQuery;
    
    private EmptyModificableTuplePanel emptyModificableTuplePanel;
    
    private Button insertButton;
    
    private TabPanel tabPanel;
    
    public InsertionPanel(ModificableSQLQuery modificableSQLQuery, TabPanel tablePanel) {
        this.modificableSQLQuery = modificableSQLQuery;
        this.emptyModificableTuplePanel = new EmptyModificableTuplePanel(modificableSQLQuery, tablePanel);
        this.insertButton = new Button("INSERTAR NUEVO REGISTRO");
        this.tabPanel = tablePanel;
        
        this.setLayout(new BorderLayout());
        this.add(this.emptyModificableTuplePanel, BorderLayout.CENTER);
        this.add(this.insertButton, BorderLayout.SOUTH);
        
        this.insertButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try {
                    InsertionPanel.this.modificableSQLQuery.insertRow(emptyModificableTuplePanel.getValues());
                    InsertionPanel.this.tabPanel.updateScrollPanel();                    
                    InsertionPanel.this.tabPanel.updateInsertionPanel(); 
                    JOptionPane.showMessageDialog(null, 
                            "Se ha insertado el registro con éxito.", 
                            "Registro insertado", 
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, 
                            "Ha habido un error en la inserción.", 
                            "Registro no insertado", 
                            JOptionPane.INFORMATION_MESSAGE); 
                } catch (DateException ex) {
                    JOptionPane.showMessageDialog(null, 
                            "Una de las fechas introducidas es incorrecta (" + ex + ")\nLas fechas deben de seguir el siguiente formato YYYY-MM-DD", 
                            "Fecha Incorrecta", 
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (NullNotPermissibleException ex) {
                     JOptionPane.showMessageDialog(null, 
                            "El campo (" + ex +") no puede contener nulos.", 
                            "Valor nulo", 
                            JOptionPane.INFORMATION_MESSAGE);
                } catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(null, 
                            "El campo (Límite de crédito) debe de ser un número.", 
                            "Valor no númerico", 
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
    }
    
}
