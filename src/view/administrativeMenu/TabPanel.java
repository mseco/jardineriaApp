package view.administrativeMenu;

import data.ModificableSQLQuery;
import java.awt.*;
import javax.swing.*;
import view.scrollPanel.ScrollPanel;

public class TabPanel extends JPanel {

    private ModificableSQLQuery modificableSQLQuery; 
    
    private JPanel scrollPanelWrapper;
    
    private CardLayout scrollPanelCardLayout;
    
    private String scrollPanelCardLayoutState;
    
    private JPanel insertionPanelWrapper;  
    
    private CardLayout insertionPanelCardLayout;
    
    private String insertionPanelCardLayoutState;    
    
    public TabPanel(ModificableSQLQuery modificableSQLQuery) {        
        this.modificableSQLQuery = modificableSQLQuery;
        this.scrollPanelCardLayout = new CardLayout();
        this.scrollPanelWrapper = new JPanel();
        this.scrollPanelWrapper.setLayout(this.scrollPanelCardLayout);
        this.scrollPanelWrapper.add(new ScrollPanel(modificableSQLQuery, this), "HEAD");
        this.scrollPanelCardLayoutState = "HEAD";
        
        this.insertionPanelCardLayout = new CardLayout();
        this.insertionPanelWrapper = new JPanel();
        this.insertionPanelWrapper.setLayout(this.insertionPanelCardLayout);
        this.insertionPanelWrapper.add(new InsertionPanel(modificableSQLQuery, this), "HEAD");    
        this.insertionPanelCardLayoutState = "HEAD";        
        
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0; 
        c.gridy = 0; 
        c.gridwidth = 3; 
        c.gridheight = 2; 
        c.weighty = 1.0;
        c.weightx = 1.0;        
        c.fill = GridBagConstraints.BOTH;
        this.add(this.scrollPanelWrapper, c);
        c.gridx = 1;
        c.gridy = 2; 
        c.gridwidth = 1; 
        c.gridheight = 1; 
        c.weighty = 0;
        c.weightx = 0.5;        
        c.insets = new Insets(8,0,0,0);
        c.fill = GridBagConstraints.BOTH;        
        this.add(this.insertionPanelWrapper, c);    
    }

    public void updateScrollPanel(){
        if(this.scrollPanelCardLayoutState.equals("HEAD")){
            this.scrollPanelWrapper.add(new ScrollPanel(modificableSQLQuery, this), "TAIL");
            this.scrollPanelCardLayout.show(this.scrollPanelWrapper, "TAIL");
            this.scrollPanelCardLayoutState = "TAIL";
        }else{
            this.scrollPanelWrapper.add(new ScrollPanel(modificableSQLQuery, this), "HEAD");
            this.scrollPanelCardLayout.show(this.scrollPanelWrapper, "HEAD");
            this.scrollPanelCardLayoutState = "HEAD";        
        }
    }

    public void updateInsertionPanel() {
       if(this.insertionPanelCardLayoutState.equals("HEAD")){
            this.insertionPanelWrapper.add(new InsertionPanel(modificableSQLQuery, this), "TAIL");
            this.insertionPanelCardLayout.show(this.insertionPanelWrapper, "TAIL");
            this.insertionPanelCardLayoutState = "TAIL";
        }else{
            this.insertionPanelWrapper.add(new InsertionPanel(modificableSQLQuery, this), "HEAD");
            this.insertionPanelCardLayout.show(this.insertionPanelWrapper, "HEAD");
            this.insertionPanelCardLayoutState = "HEAD";        
        }
    }
    
}
