package view.administrativeMenu;

import data.QueryCollection;
import data.ModificableSQLQuery;
import java.util.Iterator;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AdministrativeTabbedPanel extends JTabbedPane{

    public AdministrativeTabbedPanel() {
        for (Iterator iterator = QueryCollection.instance().ModificableSQLQueryIterator(); iterator.hasNext();) {
            ModificableSQLQuery next =(ModificableSQLQuery) iterator.next();
            this.addTab(next.getId(), new TabPanel(next));
        }
        
        this.addChangeListener(new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent e) {
                TabPanel tabPanelSelected = (TabPanel)AdministrativeTabbedPanel.this.getSelectedComponent();
                tabPanelSelected.updateScrollPanel();
            }
        });
    }
    
}
