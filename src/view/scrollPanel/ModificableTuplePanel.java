package view.scrollPanel;

import data.*;
import exceptions.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;
import view.administrativeMenu.TabPanel;

class ModificableTuplePanel extends TuplePanel{
    
    protected ModificableSQLQuery modificableSQLQuery;
    
    protected TabPanel tabPanel;

    public ModificableTuplePanel(int rowNumber, ModificableSQLQuery modificableSQLQuery, TabPanel tabPanel) {
        super(rowNumber);
        this.modificableSQLQuery = modificableSQLQuery;
        this.tabPanel = tabPanel;
        this.createFields();
    }
    
    @Override
    protected void createFields() {
        this.setLayout(new GridLayout(1, modificableSQLQuery.getColumnCount()+1));        
        for (int i = 1; i <= modificableSQLQuery.getColumnCount(); i++) {
            String fieldValue = modificableSQLQuery.getField(rowNumber, i);            
            if(modificableSQLQuery.isPrimayKey(i)){
                JTextField field = this.createJTextField(fieldValue, modificableSQLQuery.getFieldPrecision(i), false);  
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);                
            }else if(modificableSQLQuery.isForeignKey(i)){
                Filter filter= this.modificableSQLQuery.getFilterForeingKey(i);
                JComboBox field = this.createJComboBox(filter.getFilterValues(), filter.getRowNumberFromValue(fieldValue));
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);       
            }else{
                JTextField field = this.createJTextField(fieldValue, modificableSQLQuery.getFieldPrecision(i), true);  
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);                  
            }
        }
        this.addOptions();        
    }    
    
    protected JComboBox createJComboBox(String[] values, int selectedItem){
        JComboBox field = new JComboBox(values);
        field.setMaximumRowCount(5);
        field.setSelectedIndex(selectedItem-1);
        field.setEditable(false);        
        return field;
    }
    
    private void addOptions(){
        JPanel buttons = new JPanel();
        JButton updateButton = new JButton("A");  
        JButton deleteButton= new JButton("E");   
        updateButton.setBackground(Color.green);        
        deleteButton.setBackground(Color.red);        
        
        updateButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                ModificableTuplePanel.this.updateButtonActionPerformed(e);
            }
        });        
        deleteButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                ModificableTuplePanel.this.deleteButtonActionPerformed(e);
            }
        });            

        buttons.setLayout(new GridLayout());
        buttons.add(updateButton);
        buttons.add(deleteButton);
        
        this.add(buttons);
    }

    private void updateButtonActionPerformed(ActionEvent e) {
        String[] values = new String[modificableSQLQuery.getColumnCount()];
        for (int i = 1; i <= values.length; i++) {
            if(modificableSQLQuery.isForeignKey(i)){
                JComboBox field = (JComboBox)this.fieldList.get(modificableSQLQuery.getHeaderField(i));
                values[i-1] = (String)field.getSelectedItem(); 
            }else {
                JTextField field = (JTextField)this.fieldList.get(modificableSQLQuery.getHeaderField(i));
                values[i-1] = field.getText();
            }
        }
        
        try {
            this.modificableSQLQuery.updateRow(rowNumber, values);
            JOptionPane.showMessageDialog(null, 
                            "El registro se ha actualizado correctamente.", 
                            "Registro actualizado", 
                            JOptionPane.INFORMATION_MESSAGE);             
            this.tabPanel.updateScrollPanel();            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                            "Ha habido un error en la actualización.", 
                            "Registro no actualizado", 
                            JOptionPane.INFORMATION_MESSAGE);    
        } catch (DateException ex) {
            JOptionPane.showMessageDialog(null, 
                            "Una de las fechas introducidas es incorrecta (" + ex + ")\nLas fechas deben de seguir el siguiente formato YYYY-MM-DD", 
                            "Fecha Incorrecta", 
                            JOptionPane.INFORMATION_MESSAGE);
        } catch (NullNotPermissibleException ex) {
            JOptionPane.showMessageDialog(null, 
                            "El campo (" + ex +") no puedo estar vacío ni contener nulos.", 
                            "Valor nulo", 
                            JOptionPane.INFORMATION_MESSAGE);
        }
                
    }
         
    private void deleteButtonActionPerformed(ActionEvent e) {
        int reply = JOptionPane.showConfirmDialog(null, 
                            "¿Estás seguro de que quieres eliminar este registro?", 
                            "Eliminar registro", 
                            JOptionPane.INFORMATION_MESSAGE); 
        if (reply == JOptionPane.YES_OPTION) {
            this.modificableSQLQuery.deleteRow(rowNumber);
            this.tabPanel.updateScrollPanel();
        }
    }
    
}
