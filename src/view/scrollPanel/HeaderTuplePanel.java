package view.scrollPanel;

import data.*;
import java.awt.*;
import javax.swing.JTextField;

class HeaderTuplePanel extends TuplePanel {

    private SQLQuery sqlQuery;
    
    public HeaderTuplePanel(ModificableSQLQuery modificableSQLQuery) {
        super(0);
        this.sqlQuery = modificableSQLQuery;
        this.setLayout(new GridLayout(1, sqlQuery.getColumnCount()+1));
        this.createFields();
        this.add(createJTextField("", 20, false));
    }
    
    public HeaderTuplePanel(UnModificableSQLQuery unmodificableSQLQuery) {
        super(0);
        this.sqlQuery = unmodificableSQLQuery;
        this.setLayout(new GridLayout(1, sqlQuery.getColumnCount()));
        this.createFields();
    }    
    
    @Override
    protected void createFields() {
        for (int i = 1; i <= sqlQuery.getColumnCount(); i++) {
            String fieldValue = sqlQuery.getHeaderField(i);
            JTextField field = createJTextField(fieldValue, fieldValue.length(), false);
            field.setFont(new Font("Arial Bold", Font.BOLD, 12));
            fieldList.put(sqlQuery.getHeaderField(i), field);
            this.add(field);
        }
    }
}
