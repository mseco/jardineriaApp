package view.scrollPanel;

import view.utils.JTextFieldLimit;
import java.util.HashMap;
import javax.swing.*;

abstract class TuplePanel extends JPanel{

    protected int rowNumber;
    
    protected HashMap<String, JComponent> fieldList;
    
    protected final int FIELD_SIZE = 8;
    
    public TuplePanel(int rowNumber) {
        this.rowNumber = rowNumber;
        this.fieldList = new HashMap();
    }
    
    protected abstract void createFields();
    
    protected JTextField createJTextField(String textField, int maxColumnsField, boolean editable){
        JTextField field = new JTextField(FIELD_SIZE);
        field.setDocument(new JTextFieldLimit(maxColumnsField));
        field.setText(textField);
        field.setEditable(editable);
        return field;
    }    
    
}
