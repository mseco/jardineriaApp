package view.scrollPanel;

import data.ModificableSQLQuery;
import data.UnModificableSQLQuery;
import java.awt.GridLayout;
import javax.swing.JPanel;
import view.administrativeMenu.TabPanel;

class TuplesPanelBuilder {
    
    protected JPanel tuplesPanel;
    
    public JPanel build(ModificableSQLQuery modificableSQLQuery, TabPanel tabPanel){
        this.tuplesPanel = new JPanel();
        this.tuplesPanel.setLayout(new GridLayout(modificableSQLQuery.getRowCount(), modificableSQLQuery.getColumnCount()));
        for (int i = 1; i <= modificableSQLQuery.getRowCount(); i++) {
            this.tuplesPanel.add(new ModificableTuplePanel(i, modificableSQLQuery, tabPanel));
        }
        return this.tuplesPanel;     
    }
    
    public JPanel build(UnModificableSQLQuery unModificableSQLQuery){
        this.tuplesPanel = new JPanel();
        this.tuplesPanel.setLayout(new GridLayout(unModificableSQLQuery.getRowCount(), unModificableSQLQuery.getColumnCount()));
        for (int i = 1; i <= unModificableSQLQuery.getRowCount(); i++) {
            this.tuplesPanel.add(new UnModificableTuplePanel(i, unModificableSQLQuery));
        }
        return this.tuplesPanel;   
    }    
}
