package view.scrollPanel;

import data.*;
import javax.swing.*;
import view.administrativeMenu.TabPanel;

public class ScrollPanel extends JScrollPane{
    
    private ScrollPanel(){
        this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    }
        
    public ScrollPanel(ModificableSQLQuery modificableSQLQuery, TabPanel tabPanel) {
        this();
        modificableSQLQuery.refreshQuery();
        this.setViewportView(new TuplesPanelBuilder().build(modificableSQLQuery, tabPanel));
        this.setColumnHeaderView(new HeaderTuplePanel(modificableSQLQuery));
    }
    
     public ScrollPanel(UnModificableSQLQuery unModificableSQLQuery) {
        this();
        unModificableSQLQuery.refreshQuery();        
        this.setViewportView(new TuplesPanelBuilder().build(unModificableSQLQuery));
        this.setColumnHeaderView(new HeaderTuplePanel(unModificableSQLQuery));
    }

}
