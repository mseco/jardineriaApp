package view.scrollPanel;

import data.Filter;
import data.ModificableSQLQuery;
import java.awt.*;
import java.text.ParseException;
import javax.swing.*;
import javax.swing.text.MaskFormatter;
import view.administrativeMenu.TabPanel;
import view.utils.*;

public class EmptyModificableTuplePanel extends ModificableTuplePanel{
    
    public EmptyModificableTuplePanel(ModificableSQLQuery modificableSQLQuery, TabPanel tabPanel) {
        super(0, modificableSQLQuery, tabPanel);
    }
    
    @Override
    protected void createFields() {       
        this.setLayout(new GridLayout(1, this.modificableSQLQuery.getColumnCount()));        
        for (int i = 1; i <= modificableSQLQuery.getColumnCount(); i++) {
            if(modificableSQLQuery.isPrimayKey(i)){
                JTextField field = this.createJTextField(modificableSQLQuery.getNextPrimaryKeyValueAvailable(), modificableSQLQuery.getFieldPrecision(i), false);
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);                
            }else if(modificableSQLQuery.isForeignKey(i)){
                Filter filter= this.modificableSQLQuery.getFilterForeingKey(i);
                JComboBox field = this.createJComboBox(filter.getFilterValues(), 1);
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);       
            }else if(modificableSQLQuery.isDateType(i)){
                JFormattedTextField field = this.createJFormattedTextField();
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);                   
            }else{
                JTextField field = this.createJTextField("", modificableSQLQuery.getFieldPrecision(i), true); 
                TextPrompt prompt = new TextPrompt(modificableSQLQuery.getHeaderField(i), field);                    
                fieldList.put(modificableSQLQuery.getHeaderField(i), field);
                this.add(field);                    
            }
        }
    }    
    
    protected JFormattedTextField createJFormattedTextField(){
        JFormattedTextField jFormattedTextField = new JFormattedTextField();     
        try {
            MaskFormatter dateMask = new MaskFormatter("####-##-##");
            dateMask.setPlaceholderCharacter('_');           
            dateMask.install(jFormattedTextField);
        } catch (ParseException ex) {
            System.out.println(ex);
        }        
        return jFormattedTextField;
    }    
    
    public String[] getValues() {
        String[] values = new String[modificableSQLQuery.getColumnCount()];
        for (int i = 1; i <= values.length; i++) {
            if(modificableSQLQuery.isForeignKey(i)){
                JComboBox field = (JComboBox)this.fieldList.get(modificableSQLQuery.getHeaderField(i));
                values[i-1] = (String)field.getSelectedItem(); 
            }else if(modificableSQLQuery.isDateType(i)){
                JFormattedTextField field = (JFormattedTextField)this.fieldList.get(modificableSQLQuery.getHeaderField(i));
                values[i-1] = (String )field.getText();                
            }else{    
                JTextField field = (JTextField)this.fieldList.get(modificableSQLQuery.getHeaderField(i));
                values[i-1] = field.getText();
            }
        }
        return values;
    }

}
