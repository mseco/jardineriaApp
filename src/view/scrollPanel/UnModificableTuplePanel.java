package view.scrollPanel;

import java.awt.GridLayout;
import javax.swing.JTextField;
import data.UnModificableSQLQuery;
import view.scrollPanel.TuplePanel;

class UnModificableTuplePanel extends TuplePanel {

    private UnModificableSQLQuery unModificableSQLQuery;
    
    public UnModificableTuplePanel(int rowNumber, UnModificableSQLQuery unModificableSQLQuery) {
        super(rowNumber);
        this.unModificableSQLQuery = unModificableSQLQuery;
        this.setLayout(new GridLayout(1, unModificableSQLQuery.getColumnCount()));
        this.createFields();
    }
    
    @Override
    protected void createFields() {
        for (int i = 1; i <= unModificableSQLQuery.getColumnCount(); i++) {
            String fieldValue = unModificableSQLQuery.getField(rowNumber, i);
            JTextField field = createJTextField(fieldValue, fieldValue.length(), false);
            fieldList.put(unModificableSQLQuery.getHeaderField(i), field);
            this.add(field);
        }
    }
}
