package view.queriesMenu;

import data.UnModificableSQLQuery;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class QueryPanel extends JPanel implements ActionListener{
    
    protected QueriesPanel queriesPanel;

    protected String id;
    
    protected JTextArea description;

    protected JButton query;
    
    protected UnModificableSQLQuery unModificableSQLQuery;
    
    public  QueryPanel(UnModificableSQLQuery unModificableSQLQuery, QueriesPanel queriesPanel){
        this.queriesPanel = queriesPanel;
        this.id = unModificableSQLQuery.getId();
        this.description = new JTextArea(unModificableSQLQuery.getDescription());
        this.description.setEditable(false);
        this.description.setFont(new Font("Arial Bold", Font.BOLD, 15));
        this.description.setLineWrap(true);
        this.description.setWrapStyleWord(true);
        this.query = new JButton("CONSULTAR");
        this.query.addActionListener(this);
        this.unModificableSQLQuery = unModificableSQLQuery;
      
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0; 
        c.gridwidth = 3; 
        c.gridheight = 2; 
        c.weighty = 1.0;
        c.weightx = 1.0;    
        c.insets= new Insets(15, 0, 0, 0);         
        c.fill = GridBagConstraints.BOTH;        
        this.add(description, c);    
        c.gridx = 0; 
        c.gridy = 2; 
        c.gridwidth = 1; 
        c.gridheight = 1; 
        c.weighty = 0;
        c.weightx = 0;     
        c.insets= new Insets(8, 0, 0, 0);             
        c.fill = GridBagConstraints.NONE;    
        this.add(query, c);           
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        queriesPanel.updateScrollPanel(unModificableSQLQuery);
    }

}
