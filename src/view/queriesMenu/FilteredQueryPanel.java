package view.queriesMenu;

import data.FilteredSQLQuery;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class FilteredQueryPanel extends QueryPanel{
    
    private FilteredSQLQuery filteredSQLQuery;
    
    private JComboBox filter;

    public  FilteredQueryPanel(FilteredSQLQuery filteredSQLQuery, QueriesPanel queriesPanel){
        super(filteredSQLQuery, queriesPanel);
        this.filteredSQLQuery = filteredSQLQuery;
        this.filter = new JComboBox(filteredSQLQuery.getFilterValues());
        
        filter.setEditable(false);
        GridBagConstraints c = new GridBagConstraints();        
        c.gridx = 2; 
        c.gridy = 2; 
        c.gridwidth = 1; 
        c.gridheight = 1; 
        c.weighty = 0;
        c.weightx = 0;   
        c.insets= new Insets(8, 0, 0, 0);          
        c.fill = GridBagConstraints.NONE;    
        this.add(filter, c);      
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        filteredSQLQuery.changeFiltro(filteredSQLQuery.getFilterValues()[filter.getSelectedIndex()]);
        queriesPanel.updateScrollPanel(unModificableSQLQuery);
    }
    
}
