package view.queriesMenu;

import data.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public  class QueriesSelectionPanel extends JPanel implements ActionListener{

    private HashMap<Integer, String> queryCollectionMap;
    
    private JButton previousQuery, nextQuery;
    
    private JPanel queryPanelWrapper;
    
    private CardLayout cardLayout;
    
    private int cardLayoutState;
    
    public  QueriesSelectionPanel(QueriesPanel queriesPanel) {
        this.queryCollectionMap = new HashMap();
        this.previousQuery = new JButton("ANTERIOR");
        this.nextQuery = new JButton("SIGUIENTE");
        this.previousQuery.addActionListener(this);
        this.nextQuery.addActionListener(this);
        this.queryPanelWrapper = new JPanel();
        this.cardLayout = new CardLayout();
        this.cardLayoutState = 1;
        this.queryPanelWrapper.setLayout(cardLayout);
        this.fillQueryPanelWrapper(queriesPanel);
        
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1; 
        c.gridwidth = 1; 
        c.gridheight = 1; 
        c.weighty = 0;
        c.weightx = 0;      
        c.insets= new Insets(0, 8, 0, 8);  
        c.fill = GridBagConstraints.BOTH;        
        this.add(previousQuery, c);    
        c.gridx = 2; 
        c.gridy = 1; 
        c.gridwidth = 1; 
        c.gridheight = 1; 
        c.weighty = 0;
        c.weightx = 0;     
        c.insets= new Insets(0, 8, 0, 8);        
        c.fill = GridBagConstraints.BOTH;    
        this.add(nextQuery, c); 
        c.gridx = 1; 
        c.gridy = 0; 
        c.gridwidth = 1; 
        c.gridheight = 3; 
        c.weighty = 1.0;
        c.weightx = 1.0;   
        c.insets= new Insets(0, 0, 0, 0);           
        c.fill = GridBagConstraints.BOTH;    
        this.add(queryPanelWrapper, c);               
    }

    private void fillQueryPanelWrapper(QueriesPanel queriesPanel) {
        int cont = 1;
        
        for (Iterator iterator = QueryCollection.instance().unModificableSQLQueryIterator(); iterator.hasNext();) {
            UnModificableSQLQuery next = (UnModificableSQLQuery) iterator.next();
            queryPanelWrapper.add(new QueryPanel(next, queriesPanel), next.getId());
            queryCollectionMap.put(cont, next.getId());
            cont++;
        }

        for (Iterator iterator = QueryCollection.instance().filteredSQLQueryIterator(); iterator.hasNext();) {
            FilteredSQLQuery next = (FilteredSQLQuery) iterator.next();
            queryPanelWrapper.add(new FilteredQueryPanel(next, queriesPanel), next.getId());
            queryCollectionMap.put(cont, next.getId());
            cont++;
        }        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("SIGUIENTE")){
            if(cardLayoutState == queryCollectionMap.size()){
                cardLayout.show(queryPanelWrapper, queryCollectionMap.get(1));
                cardLayoutState = 1;            
            }else{
                cardLayout.show(queryPanelWrapper, queryCollectionMap.get(cardLayoutState+1));
                cardLayoutState++;
            }
        }else if (e.getActionCommand().equals("ANTERIOR")){
            if(cardLayoutState == 1){
                cardLayout.show(queryPanelWrapper, queryCollectionMap.get(queryCollectionMap.size()));
                cardLayoutState = queryCollectionMap.size();            
            }else{
                cardLayout.show(queryPanelWrapper, queryCollectionMap.get(cardLayoutState-1));
                cardLayoutState--;
            }            
        }
    }

}
