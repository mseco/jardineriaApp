package view.queriesMenu;

import data.UnModificableSQLQuery;
import java.awt.*;
import javax.swing.*;
import view.scrollPanel.*;

public class QueriesPanel extends JPanel{
    
    private QueriesSelectionPanel queryListPanel;
    
    private JPanel queryCollectionPanelWrapper;
    
    private CardLayout cardLayout;
    
    private String cardLayoutState;
    
    public QueriesPanel() {
        this.queryListPanel = new QueriesSelectionPanel(this);
        this.queryCollectionPanelWrapper = new JPanel();
        this.cardLayout = new CardLayout();
        this.queryCollectionPanelWrapper.setLayout(cardLayout);
        this.cardLayoutState = "NONE";

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0; 
        c.gridwidth = 3; 
        c.gridheight = 1; 
        c.weighty = 0.2;
        c.weightx = 0.2;        
        c.fill = GridBagConstraints.BOTH;        
        this.add(queryListPanel, c);    
        c.gridx = 0; 
        c.gridy = 1; 
        c.gridwidth = 3; 
        c.gridheight = 2; 
        c.weighty = 1.0;
        c.weightx = 1.0;        
        c.fill = GridBagConstraints.BOTH;    
        this.add(queryCollectionPanelWrapper, c);        
    }
    
    public void updateScrollPanel(UnModificableSQLQuery unModificableSQLQuery){
        if(cardLayoutState.equals("NONE")){
            queryCollectionPanelWrapper.add(new JPanel(), "HEAD");            
            queryCollectionPanelWrapper.add(new ScrollPanel(unModificableSQLQuery), "TAIL");
            cardLayout.show(queryCollectionPanelWrapper, "TAIL");
            this.cardLayoutState = "TAIL";            
        }else if(cardLayoutState.equals("HEAD")){
            queryCollectionPanelWrapper.add(new ScrollPanel(unModificableSQLQuery), "TAIL");
            cardLayout.show(queryCollectionPanelWrapper, "TAIL");
            this.cardLayoutState = "TAIL";
        }else{
            queryCollectionPanelWrapper.add(new ScrollPanel(unModificableSQLQuery), "HEAD");
            cardLayout.show(queryCollectionPanelWrapper, "HEAD");
            this.cardLayoutState = "HEAD";        
        }
    }
    
}
