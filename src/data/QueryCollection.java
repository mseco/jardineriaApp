package data;

import java.util.*;

public class QueryCollection {

    private static QueryCollection queryCollection;
    
    static public QueryCollection instance(){
        if(queryCollection == null){
            queryCollection = new QueryCollection();
        }
        return queryCollection;
    }
    
    private HashMap<String, ModificableSQLQuery> modificableQueriesMap; 
    
    private HashMap<String, UnModificableSQLQuery> unModificableQueriesMap;
    
    private HashMap<String, FilteredSQLQuery> filteredQueriesMap;
    
    private QueryCollection(){
        QueryCollectionBuilder builder = new QueryCollectionBuilder();
        this.modificableQueriesMap = builder.createModificableQueries();
        this.unModificableQueriesMap = builder.createUnModificablQueries();
        this.filteredQueriesMap = builder.createFilteredQueries();
    }

    public Iterator ModificableSQLQueryIterator(){
        return this.modificableQueriesMap.values().iterator();
    }
    
    public Iterator unModificableSQLQueryIterator(){
        return this.unModificableQueriesMap.values().iterator();
    }
    
    public Iterator filteredSQLQueryIterator(){
        return this.filteredQueriesMap.values().iterator();
    }
    
}
