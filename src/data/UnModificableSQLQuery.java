package data;

import java.sql.PreparedStatement;

public class UnModificableSQLQuery extends SQLQuery {

    private String description;
    
    public UnModificableSQLQuery(PreparedStatement preparedStatement, String description, String id){
        super(preparedStatement, id);
        this.description=description;
    }
    
    public String getDescription(){
        return this.description;
    }
}
