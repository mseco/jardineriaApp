package data;

import util.Connection;
import java.sql.*;
import javax.swing.JOptionPane;

public class Filter {

    private SQLQuery sqlQuery;

    private String dataType;
    
    private boolean hasNull;
    
    public Filter(String statement, String dataType, String id) {
        this.sqlQuery = new SQLQuery(Connection.instance().prepareStatement(statement), id);
        this.sqlQuery.refreshQuery();  
        this.dataType = dataType;
        this.hasNull = false;
    }
    
    void filter(String filtroValue, PreparedStatement preparedStatement) {
        try {
            switch(this.dataType){
                case "int":
                    preparedStatement.setInt(1, Integer.parseInt(filtroValue));
                    break;
                case "String":
                    preparedStatement.setString(1, filtroValue);
                    break;
                 }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
    }
    
    void addNull() {
        this.hasNull = true;
    }
    
    public String[] getFilterValues(){
        String[] values = this.sqlQuery.getColumnValues(1);
        if (this.hasNull){
            String[] valuesTemp = new String[values.length + 1];
            for (int i = 0; i < values.length; i++) {
                valuesTemp[i] = values[i];
            }
            valuesTemp[valuesTemp.length-1] = "null";
            return valuesTemp;            
        }else{
            return values;
        }
    }
    
    public int getRowNumberFromValue(String value){
        for (int i = 1; i <= this.sqlQuery.getRowCount(); i++) {
            if(this.sqlQuery.getField(i, 1).equals(value)){
                return i;
            }
        }
        return sqlQuery.getRowCount()+1;
    }
    
    public void refreshFilter(){
        this.sqlQuery.refreshQuery();    
    }
    
}
