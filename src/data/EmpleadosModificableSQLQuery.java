package data;

import exceptions.*;
import util.Connection;
import java.sql.*;

public class EmpleadosModificableSQLQuery extends ModificableSQLQuery {
    
    public EmpleadosModificableSQLQuery() {
        super(Connection.instance().prepareStatement("SELECT * FROM EMPLEADOS", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE), "Empleados");
    }

    @Override
    public void updateFields(String[] rowFields) throws SQLException, DateException, NullNotPermissibleException {
        //CodigoEmpleado PK
        this.resultSet.updateInt(1, Integer.parseInt(rowFields[0]));
        
        //Nombre
        this.checkIsNotNull(2, rowFields[1]);
        this.resultSet.updateString(2, rowFields[1]);
        
        //Apellido1
        this.checkIsNotNull(3, rowFields[2]);
        this.resultSet.updateString(3, rowFields[2]);
        
        //Apellido2
        if(rowFields[3].equals("null")){
            this.resultSet.updateNull(4);                   
        }else{
            this.resultSet.updateString(4, rowFields[3]);
        }        
        
        //Extension
        this.checkIsNotNull(5, rowFields[4]);            
        this.resultSet.updateString(5, rowFields[4]); 
        
        //Email
        this.checkIsNotNull(6, rowFields[5]);            
        this.resultSet.updateString(6, rowFields[5]); 
        
        //CodigoOficina FK
        this.resultSet.updateString(7, rowFields[6]); 
        
        //CodigoJefe FK
        if(rowFields[7].equals("null")){
            this.resultSet.updateNull(8);
        }else{
            this.resultSet.updateInt(8, Integer.parseInt(rowFields[7]));            
        }
        
        //Puesto
        if(rowFields[8].equals("null")){
            this.resultSet.updateNull(9);                   
        }else{
            this.resultSet.updateString(9, rowFields[8]); 
        }            
 
    }
    
    @Override
    public boolean isPrimayKey(int fieldNumber) {
        return fieldNumber == 1;
    }

    @Override
    public boolean isForeignKey(int fieldNumber) {
        return fieldNumber == 7 || fieldNumber == 8;
    }

    @Override
    public boolean isDateType(int fieldNumber) {
        return false;
    }

    @Override
    public Filter getFilterForeingKey(int fieldNumber){
        assert fieldNumber == 7 || fieldNumber == 8;
        if(fieldNumber == 7){
            return new Filter("SELECT DISTINCT codigoOficina FROM oficinas ORDER BY codigoOficina", "string", "CodigoOficina");
        }else{
            Filter filter = new Filter("SELECT DISTINCT codigoEmpleado FROM empleados ORDER BY codigoEmpleado", "int", "CodigoJefe");   
            filter.addNull();
            return filter;       
        }
    }

}
