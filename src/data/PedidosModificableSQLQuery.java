package data;

import exceptions.*;
import util.Connection;
import java.sql.*;
import java.text.*;

public class PedidosModificableSQLQuery extends ModificableSQLQuery {
    
    public PedidosModificableSQLQuery() {
        super(Connection.instance().prepareStatement("SELECT * FROM PEDIDOS", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE), "Pedidos");
    }

    @Override
    public void updateFields(String[] rowFields) throws SQLException, DateException, NullNotPermissibleException {
        //CodigoPedido PK
        this.resultSet.updateInt(1, Integer.parseInt(rowFields[0]));
        
        //FechaPedido
        this.checkIsNotNull(2, rowFields[1]);
        this.resultSet.updateDate(2, this.stringToDate(rowFields[1]));
        
        //FechaEsperada
        this.checkIsNotNull(3, rowFields[2]);
        this.resultSet.updateDate(3, this.stringToDate(rowFields[2])); 
        
        //FechaEntrega
        if((rowFields[3].equals("null") || rowFields[3].isEmpty() || (rowFields[3].equals("____-__-__")))){
            this.resultSet.updateNull(4);                   
        }else{
            this.resultSet.updateDate(4, this.stringToDate(rowFields[3]));
        }
        
        //Estado
        this.checkIsNotNull(5, rowFields[4]);
        this.resultSet.updateString(5, rowFields[4]);
        
        //Comentarios
        if(rowFields[5].equals("null")){
            this.resultSet.updateNull(6);                   
        }else{
            this.resultSet.updateString(6, rowFields[5]);
        }
        
        //CodigoCliente FK
        this.resultSet.updateInt(7, Integer.parseInt(rowFields[6]));
    }    
    
    private java.sql.Date stringToDate(String stringDate) throws DateException {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return new java.sql.Date(formatter.parse(stringDate).getTime());
        } catch (ParseException ex) {
            throw new DateException(stringDate);
        }
    }
    
    @Override
    public boolean isPrimayKey(int fieldNumber) {
        return fieldNumber == 1;
    }

    @Override
    public boolean isForeignKey(int fieldNumber) {
        return fieldNumber == 7;
    }

    @Override
    public boolean isDateType(int fieldNumber) {
        return fieldNumber == 2 || fieldNumber == 3 || fieldNumber == 4;
    }
    
    @Override
    public Filter getFilterForeingKey(int fieldNumber) {
        assert fieldNumber == 7;
        return new Filter("SELECT DISTINCT codigoCliente FROM clientes ORDER BY codigoCliente", "int", "CodigoCliente");
    }

}
