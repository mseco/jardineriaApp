package data;

import util.Connection;
import java.util.*;

public class QueryCollectionBuilder {

    private HashMap<String, ModificableSQLQuery> modificableQueriesMap;

    private HashMap<String, UnModificableSQLQuery> unModificableQueriesMap;

    private HashMap<String, FilteredSQLQuery> filteredQueriesMap;

    private Connection connection;

    public QueryCollectionBuilder() {
        this.connection = Connection.instance();
    }

    public HashMap createModificableQueries() {
        this.modificableQueriesMap = new HashMap();
        this.modificableQueriesMap.put("Empleados", new EmpleadosModificableSQLQuery());
        this.modificableQueriesMap.put("Clientes", new ClientesModificableSQLQuery());
        this.modificableQueriesMap.put("Pedidos", new PedidosModificableSQLQuery());
        return this.modificableQueriesMap;
    }

    public HashMap createUnModificablQueries() {
        this.unModificableQueriesMap = new HashMap();
        String descripcionProblema;
        String idProblema;
        String solucionProblema;
        
        idProblema = "3";
        descripcionProblema = "Mostrar el nombre de aquellos productos que no se han pedido nunca.";
        solucionProblema = "SELECT DISTINCT nombre FROM productos WHERE codigoproducto not in(SELECT DISTINCT codigoproducto FROM detallepedidos)";
        this.unModificableQueriesMap.put(idProblema, new UnModificableSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema));

        idProblema = "4";
        descripcionProblema = "Muestra el nombre y el código de cliente de aquellos clientes a los que le entregaron pedidos en el primer trimestre de 2009 ordenado por código de cliente ascendente (no deben aparecer clientes repetidos).";
        solucionProblema = "SELECT DISTINCT c.nombrecliente, c.codigocliente FROM clientes c, pedidos p WHERE c.codigocliente=p.codigocliente and p.fechaentrega between '2009/01/01' and '2009/03/31'";
        this.unModificableQueriesMap.put(idProblema, new UnModificableSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema));
        
        return this.unModificableQueriesMap;
    }

    public HashMap createFilteredQueries() {
        this.filteredQueriesMap = new HashMap();
        String descripcionProblema;
        String idProblema;
        String solucionProblema;
        Filter filtroProblema;
        
        idProblema = "1";
        descripcionProblema = "Dada una forma de pago que se introduce por pantalla, mostrar el número de pagos de importe superior a 3000€ que se han realizado a través de esa forma de pago.";
        solucionProblema = "SELECT COUNT(*) as Total_De_Pagos FROM pagos WHERE cantidad>3000 and FormaPago = ?";
        filtroProblema = new Filter("SELECT DISTINCT FormaPago FROM pagos", "String", "FormasPagos");
        this.filteredQueriesMap.put(idProblema, new FilteredSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema, filtroProblema));
        
        idProblema = "2";
        descripcionProblema = "Dado un código de producto que se introduce por pantalla, obtener el nombre, el código del pedido y la máxima cantidad de este producto que se ha pedido.";
        solucionProblema = "SELECT p.nombre as nombreProducto, d.codigoPedido, d. cantidad "
							+ "FROM productos p, detallepedidos d"
							+ " WHERE p.codigoproducto = d.codigoproducto and p.codigoproducto = ?"
							+ " and d.cantidad =(select max(cantidad)"
							+			" from detallepedidos"
							+			" group by codigoproducto"
							+			" having codigoproducto = p.codigoproducto)";
        filtroProblema =  new Filter("SELECT DISTINCT codigoProducto FROM productos", "String", "CodigoProductos");
        this.filteredQueriesMap.put(idProblema, new FilteredSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema, filtroProblema));
        
        idProblema = "5";
        descripcionProblema = "Calcular el importe total de los pedidos realizados por los clientes asignados al representante de ventas con el número de empleado que se introducirá por pantalla.";
        solucionProblema = "SELECT sum(d.cantidad)as ImporteTotal FROM empleados e, clientes c, pedidos p, detallepedidos d " 
               + "WHERE e.codigoempleado=c.codigoempleadorepventas and c.codigocliente=p.codigocliente and p.codigopedido=d.codigopedido and e.codigoempleado = ?";
        filtroProblema = new Filter("SELECT DISTINCT codigoempleado FROM empleados ORDER BY codigoEmpleado", "int", "CodigoEmpleados");
        this.filteredQueriesMap.put(idProblema, new FilteredSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema, filtroProblema));
        
        idProblema = "6";
        descripcionProblema = "Buscar clientes por apellido.";
        solucionProblema = "SELECT * FROM CLIENTES WHERE ApellidoContacto= ?";
        filtroProblema = new Filter("SELECT DISTINCT ApellidoContacto FROM clientes", "String", "ApellidoContacto");
        this.filteredQueriesMap.put(idProblema, new FilteredSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema, filtroProblema));
        
        idProblema = "7";
        descripcionProblema = "Buscar clientes por ciudad.";
        solucionProblema = "SELECT * FROM CLIENTES WHERE Ciudad= ?";
        filtroProblema = new Filter("SELECT DISTINCT Ciudad FROM clientes", "String", "Ciudad");
        this.filteredQueriesMap.put(idProblema, new FilteredSQLQuery(this.connection.prepareStatement(solucionProblema), descripcionProblema, idProblema, filtroProblema));
                
        return this.filteredQueriesMap;
    }

}