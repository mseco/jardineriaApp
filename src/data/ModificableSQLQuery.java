package data;

import exceptions.*;
import java.sql.*;
import javax.swing.JOptionPane;

public abstract class ModificableSQLQuery extends SQLQuery {
    
    private int nextPrimaryKeyValueAvailable;

    public ModificableSQLQuery(PreparedStatement preparedStatement, String id){
        super(preparedStatement, id);
    }
    
    public int getFieldPrecision(int columnNumber){
        try {
            return this.resultSet.getMetaData().getPrecision(columnNumber);
        } catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
        return 0;
    }
    
    public String getNextPrimaryKeyValueAvailable(){
        if (this.nextPrimaryKeyValueAvailable == 0){
            this.nextPrimaryKeyValueAvailable = Integer.parseInt(this.getField(this.getRowCount(),1));
        }
        this.nextPrimaryKeyValueAvailable++;
        return "" + this.nextPrimaryKeyValueAvailable;
    }
    
    protected void checkIsNotNull(int columnNumber, String field) throws NullNotPermissibleException, SQLException {
        if (field.equals("null") || field.isEmpty()) {
            throw new NullNotPermissibleException(this.getHeaderField(columnNumber));
        }
    }    
    
    public void deleteRow(int rowNumber){
        try {
            this.resultSet.absolute(rowNumber);
            this.resultSet.deleteRow();
        } catch (SQLException ex) {
               JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
        }
    }
    
    public void updateRow(int rowNumber, String[] rowFields)throws SQLException, DateException, NullNotPermissibleException{
        try {
            this.resultSet.absolute(rowNumber);
            this.updateFields(rowFields);
            this.resultSet.updateRow();
        } catch (NumberFormatException ex){
               JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
        }    
    }
    
    public abstract void updateFields(String[] rowFields)throws SQLException, DateException, NullNotPermissibleException;
    
    public void insertRow(String[] rowFields)throws SQLException, DateException, NullNotPermissibleException{
        try {
            this.resultSet.moveToInsertRow();
            this.updateFields(rowFields);
            this.resultSet.insertRow();
        } catch (NumberFormatException ex){
               JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
        }    
    }

    public abstract boolean isPrimayKey(int fieldNumber);

    public abstract boolean isForeignKey(int fieldNumber);

    public abstract Filter getFilterForeingKey(int fieldNumber);

    public abstract boolean isDateType(int fieldNumber);

}
