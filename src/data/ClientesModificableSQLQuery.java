package data;

import exceptions.DateException;
import exceptions.NullNotPermissibleException;
import util.Connection;
import java.sql.*;

public class ClientesModificableSQLQuery extends ModificableSQLQuery {
    
    public ClientesModificableSQLQuery(){
        super(Connection.instance().prepareStatement("SELECT * FROM CLIENTES", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE), "Clientes");
    }

    @Override
    public void updateFields(String[] rowFields) throws SQLException, DateException, NullNotPermissibleException, NumberFormatException {
        //CodigoCliente PK 
        this.resultSet.updateInt(1, Integer.parseInt(rowFields[0]));     
        
        //NombreCliente
        this.checkIsNotNull(2, rowFields[1]);
        this.resultSet.updateString(2, rowFields[1]); 
        
        //NombreContacto
        if(rowFields[2].equals("null")){
            this.resultSet.updateNull(3);                   
        }else{
            this.resultSet.updateString(3, rowFields[2]);
        }         
        
        //ApellidoContacto
        if(rowFields[3].equals("null")){
            this.resultSet.updateNull(4);                   
        }else{
            this.resultSet.updateString(4, rowFields[3]); 
        }        
        
        //Telefono
        this.checkIsNotNull(5, rowFields[4]);
        this.resultSet.updateString(5, rowFields[4]); 
        
        //Fax
        this.checkIsNotNull(6, rowFields[5]);
        this.resultSet.updateString(6, rowFields[5]); 
        
        //LineaDireccion1
        this.checkIsNotNull(7, rowFields[6]);
        this.resultSet.updateString(7, rowFields[6]); 
        
        //LineaDireccion2
        if(rowFields[7].equals("null")){
            this.resultSet.updateNull(8);                   
        }else{
            this.resultSet.updateString(8, rowFields[7]); 
        }          
        
        //Ciudad
        this.checkIsNotNull(9, rowFields[8]);
        this.resultSet.updateString(9, rowFields[8]); 
        
        //Region
        if(rowFields[9].equals("null")){
            this.resultSet.updateNull(10);                   
        }else{
            this.resultSet.updateString(10, rowFields[9]);
        }             
        
        //Pais
        if(rowFields[10].equals("null")){
            this.resultSet.updateNull(11);                   
        }else{
            this.resultSet.updateString(11, rowFields[10]); 
        }            
        
        //CodigoPostal
        if(rowFields[11].equals("null")){
            this.resultSet.updateNull(12);                   
        }else{
            this.resultSet.updateString(12, rowFields[11]); 
        }            
        
        //CodigoEmpleadoRepVentas FK
        if(rowFields[12].equals("null")){
            this.resultSet.updateNull(13);                   
        }else{
            this.resultSet.updateInt(13, Integer.parseInt(rowFields[12]));
        }            
        
        //LimiteCredito
        if(rowFields[13].equals("null")){
            this.resultSet.updateNull(14);                   
        }else{
            this.resultSet.updateDouble(14, Double.parseDouble(rowFields[13])); 
        }            
        
    }

    @Override
    public boolean isPrimayKey(int fieldNumber) {
        return fieldNumber == 1;
    }

    @Override
    public boolean isForeignKey(int fieldNumber) {
        return fieldNumber == 13;
    }

    @Override
    public boolean isDateType(int fieldNumber) {
        return false;
    }    

    @Override
    public Filter getFilterForeingKey(int fieldNumber){
        assert fieldNumber == 13;
        Filter filter = new Filter("SELECT DISTINCT codigoEmpleado FROM empleados ORDER BY codigoEmpleado", "int", "CodigoEmpleado");
        filter.addNull();
        return filter;
    }
    
}
