package data;

import java.sql.*;
import javax.swing.JOptionPane;

public class SQLQuery {

    protected PreparedStatement preparedStatement;
    
    protected ResultSet resultSet;
    
    protected String id;
    
    public SQLQuery(PreparedStatement preparedStatement, String id){
        this.preparedStatement = preparedStatement;
        this.id = id;
    }
    
    public void refreshQuery(){
        try {
            this.resultSet = this.preparedStatement.executeQuery();
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }    
    }
    
    public String getField(int rowNumber, int columnNumber){
        try {
            this.resultSet.absolute(rowNumber);
            String field = this.resultSet.getString(columnNumber);
            if(field == null){
                return "null";
            }else{
                return field;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
        }
        return null;
    }
    
    public String getHeaderField(int columnNumber){
        try {
            return this.resultSet.getMetaData().getColumnName(columnNumber);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);         
        }
        return null;
    }
    
    public String[] getColumnValues(int columnNumber){
        String[] columnValues = new String[this.getRowCount()];
        int numberOfNulls = 0;
        int currentIndexArray = 0;
        for (int i = 0; i < columnValues.length; i++) {
            if (this.getField(i+1, columnNumber).equals("null")){
                numberOfNulls++;
            }else{
                columnValues[currentIndexArray]= this.getField(i+1, columnNumber);
                currentIndexArray++;
            }
        }
        if(numberOfNulls > 0){
            String [] columnTemp = new String[this.getRowCount() - numberOfNulls];
            for (int i = 0; i < currentIndexArray; i++) {
                columnTemp[i]= columnValues[i];
            }
            columnValues = columnTemp;
        }
        return columnValues;
    }
    
    public int getRowCount(){
        int result = 0;
        try {
            if (this.resultSet.first()){
                do{
                    result++;
                }while (this.resultSet.next()); 
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);          
        }
        return result;
    }
    
    public int getColumnCount() {
        try {
            return this.resultSet.getMetaData().getColumnCount();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, 
                                "Error con la conexión a la base de datos, el programa se va a cerrar.", 
                                "Error de conexión", 
                                JOptionPane.INFORMATION_MESSAGE); 
            System.exit(0);
            return 0;
        }
    }
    
    public String getId(){        
        return this.id;
    }

}
