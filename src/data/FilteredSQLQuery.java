package data;

import java.sql.*;

public class FilteredSQLQuery extends UnModificableSQLQuery{
    
    private PreparedStatement preparedStatement;
    
    private Filter filter;
    
    public FilteredSQLQuery(PreparedStatement preparedStatement, String description, String id, Filter filter) {
        super(preparedStatement, description, id);
        this.preparedStatement = preparedStatement;
        this.filter = filter;
    }
    
    public void changeFiltro(String filtroValue) {
        this.filter.filter(filtroValue, this.preparedStatement);
    }
    
    public String[] getFilterValues(){
        return this.filter.getFilterValues();
    }

    public void refreshFilter() {
        filter.refreshFilter();
    }
}
