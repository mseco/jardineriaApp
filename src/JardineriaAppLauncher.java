import util.Connection;
import view.*;

public class JardineriaAppLauncher {

    public static void main(String[] args) {
        Connection.instance().open();
        new FrameJardineriaApp().init();           
    }
}
